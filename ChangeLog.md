# Change log

[2024.11.14] Fixed: pydecs-plot-densities

[2024.07.31] Fixed defect-identification process: pydecs-EdefCorrect-ExtFNV-VASP 

[2024.06.27] Fix misc.

[2024.05.12] Added: dos-Escissor

[2024.05.06] Fix messages and typos by cf.

[2024.04.14-15] Fix plot.

[2024.04.06] Fix pydecs-checkEqPhases

[2024.03.24] Fix plot-densities: chempot_ for x-axis and dens_merge_sites
[2024.03.24] Fix defects: different atomic sites can be categorized by [#] in site-label definition

[2024.01.15] Fix for setting direct position: pydecs-EdefCorrect-ExtFNV-VASP

[2023.12.12] Fix espot (important)

[2023.11.22] Fix output (misc.)

[2023.11.20] Fix output of fix_Natoms with scale factor

[2023.11.17,2023.11.18,2023.11.19] Added espot sweeping and fixed it

[2023.11.16] Update: output of loaded parameters

[2023.11.15] Update: plot-densities, add new formula for defect densities with a few bug-fix

[2023.11.14] Update: plot-densities

[2023.11.11] Fixed: print-out & error-check in defects, and pressures

[2023.10.28] Fixed: pydecs-EdefCorrect-ExtFNV-VASP 

[2023.09.29] Modified plots: scaling size-dependent data (dens_scale in pydecs.toml)
[2023.09.29] Modified plots: densities of same species can be merged (dens_merge in pydecs.toml)

[2023.09.28] Added: exaamples/Mg2Si/test03_fixLiNaConcentration

[2023.07.25] Fixed: pydecs-plot-densities

[2023.06.30] Fixed: pydecs-convDOS-VASP

[2023.06.29] Modified: input-[[host.site]] occ_atom="NONE"

[2023.06.06,2023.06.04,2023.06.02] Fixed: pydecs-EdefCorrect-ExtFNV-VASP

[2023.03.22] Fixed-tag in inpydecs.toml: min(max) to start(end) for parameter-range setting

[2023.02.09] Fixed: pydecs-plot-densities

[2022.11.17] Fixed: pydecs-checkEqPhases

[2022.11.07] Fixed: Solver for fix-Natoms and pydecs-EdefCorrect-ExtFNV-VASP

[2022.11.05] Fixed: Error handling for minus defect formation energies within the bandgap

[2022.11.04] complex defects can be handled in pydecs-EdefCorrect-ExtFNV-VASP. Fixed: Error handling for minus defect formation energies within the bandgap

[2022.09.22] Fixed: pydecs-EdefCorrect-ExtFNV-VASP

[2022.09.05] Added a new tool: pydecs-EdefCorrect-ExtFNV-VASP

[2022.03.07] 1st distribution version
