# pydecs

PYthon code for point-like Defect Equilibria in Crystalline Solids

For details, check belows:  
- paper: [T. Ogawa, A. Taguchi, and A. Kuwabara, "An extended computational approach for point-defect equilibria in semiconductor materials", npj Comput. Mater. 8, 79 (2022).](https://doi.org/10.1038/s41524-022-00756-0)  
- [User manual](https://gitlab.com/tkog/pydecs/wikis/pydecs-(Main-page))


This module is licensed under the Apache License, Version 2.0, see LICENSE.txt.
